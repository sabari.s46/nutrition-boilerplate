import React, { Component } from "react";

export default class FoodBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 0,
      inputValue: 0, // added inputValue to store input field value
    };
  }

  handleQuantityChange() {
    // Get the input field value and add it to the current quantity
    const { inputValue } = this.state;
    this.setState({ quantity: inputValue });
  }

  handleInputChange(e) {
    // Update the inputValue when the input field changes
    this.setState({ inputValue: parseInt(e.target.value, 10) || 0 });
  }

  reset() {
    this.setState({ inputValue: 0, quantity: 0 });
  }

  render() {
    const { name, cal, img } = this.props;
    const { quantity } = this.state;
    const totalCalories = cal * quantity;

    return (
      <div className="box">
        <article className="media">
          <div className="media-left">
            <figure className="image is-64x64">
              <img src={img} alt={name} />
            </figure>
          </div>
          <div className="media-content">
            <div className="content">
              <p>
                <strong>{name}</strong> <br />
                <small>{cal} cal</small>
              </p>
            </div>
          </div>
          <div className="media-right">
            <div className="field has-addons">
              <div className="control">
                <input
                  className="input"
                  type="number"
                  value={this.state.inputValue}
                  onChange={(e) => this.handleInputChange(e)}
                />
              </div>
              <div className="control">
                <button
                  className="button is-info"
                  onClick={() => this.handleQuantityChange()}
                >
                  +
                </button>
              </div>
            </div>
          </div>
        </article>

        <div className="calCount">{`${quantity} ${name} = ${totalCalories} calories`}</div>

        <div className="resetBtn" onClick={() => this.reset()}>
          Reset
        </div>
      </div>
    );
  }
}
